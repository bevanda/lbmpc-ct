# Learning-based MPC using [ct](https://github.com/ethz-adrl/control-toolbox)

Implementing learning based MPC capable to run online

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### System requirements
* Ubuntu 16.04 or 18.04
* compiler supporting C++14

### Installing dependencies

Firstly installing [eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)
```
sudo apt-get update
```
```
sudo apt-get install libeigen3-dev 
```
and, also, boost ia required
```
sudo apt-get install libboost-all-dev
```
For installing in a catkin workspace, [catkin_tools](https://catkin-tools.readthedocs.io/en/latest/installing.html) are required.<br/>
Furthermore, NLP solving [IPOPT](https://www.coin-or.org/Ipopt/documentation/) is required, installed with a convenient debian packaging:
```
sudo apt-get install coinor-libipopt-dev
```
#### In your catkin workspace:

* clone [kindr](https://github.com/ANYbotics/kindr) linear algebra library
* clone [control-toolbox](https://github.com/ethz-adrl/control-toolbox) optimal control library (NOTE: put a CATKIN_IGNORE in the /ct_doc folder to build w/o error)
#### Build tool

* [catkin build]

#### Building the package
To have everything set up out of the box, run 

```
catkin build -DCMAKE_BUILD_TYPE=Release
```

in your workspace folder.
## Running the examples
After building make sure the workspace is sourced by

```
source your_catkin_ws/devel/setup.bash
```
Then you are able to run all of the example nodes i.e.
```
rosrun lbmpc-ct NMPC_continuous_test_node 
```

## Deployment

TODO: Add additional notes about how to deploy this on a real system


## Contributing
TODO

## Versioning
TODO

## Authors

* **Petar Bevanda** 

## License
TODO

## Acknowledgments
TODO


