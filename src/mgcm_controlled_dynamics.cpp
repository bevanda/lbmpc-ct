#include <ct/core/core.h>
#include "models/MGCMctrld.h"
#include "controllers/PID.h"

// using namespace sys;

int main(int argc, char** argv)
{
    // a a Moore-Greitzer compressor model has 4 states:
    //  mass flow, pressure rise, throttle input, throttle rate
    const size_t state_dim = MGCM::STATE_DIM;  // = 4
    const size_t ctrl_dim = MGCM::CONTROL_DIM;  // = 1

    // create state and input objects
    ct::core::StateVector<state_dim> x;
    double u;
    // init states
    x(0) = 0.5; 
    x(1) = 1.6875;
    x(2) = 1.1547; 
    x(3) = 0;
    // create our MGCM instance
    double x2_c = 0;
    double beta = 1.0;
    double zeta = 1.0/sqrt(2.0);
    double wn = sqrt(1000);
    std::shared_ptr<MGCM> compressor(new MGCM(x2_c, beta, zeta, wn));
    // create our controller
    double kp = 10;
    double kd = 1;
    ct::core::ControlVector<ctrl_dim> uff;
    uff << 1.1547;
    std::shared_ptr<PID> controller(new PID(uff, kp, kd));
    // assign our controller
    compressor->setController(controller);
    // create an integrator
    ct::core::Integrator<state_dim> integrator(compressor);
    // simulate 10s steps
    ct::core::Time t0 = 0.0;
    for (size_t i = 0; i < 10; i++) 
    {
        double dt = 0.01;
        size_t nSteps = 100;
        integrator.integrate_n_steps(x, t0, nSteps, dt);
        // print the new state
        std::cout << "state after integration: " << x.transpose() << std::endl;
        t0+=dt*nSteps;
    }
    // plotResultsMGCM<state_dim, ctrl_dim>(solution.x_ref(), solution.uff(), solution.time());

    return 0;
}