#include <ct/core/core.h>
#include "models/MGCM.h"

int main(int argc, char** argv)
{
    // a damped oscillator has two states, position and velocity
    const size_t state_dim = MGCM::STATE_DIM;  // = 4
    const size_t ctrl_dim = MGCM::CTRL_DIM;  // = 1

    // create state and input objects
    ct::core::StateVector<state_dim> x;
    double u;
    // init states
    x(0) = 0.5; 
    x(1) = 1.6875;
    x(2) = 1.1547; 
    x(3) = 0;
    // init input
    u = 1.1547;
    // create our MGCM instance
    double x2_c = 0;
    double beta = 1.0;
    double zeta = 1.0/sqrt(2.0);
    double wn = sqrt(1000);
    std::shared_ptr<MGCM> mgcm(new MGCM(x2_c, beta, zeta, wn, u));
    // create an integrator
    ct::core::Integrator<state_dim> integrator(mgcm);
    // simulate 10s steps
    ct::core::Time t0 = 0.0;
    for (size_t i = 0; i < 10; i++) 
    {
        double dt = 0.01;
        size_t nSteps = 100;
        integrator.integrate_n_steps(x, t0, nSteps, dt);
        // print the new state
        std::cout << "state after integration: " << x.transpose() << std::endl;
        t0+=dt*nSteps;
    }
    

    return 0;
}