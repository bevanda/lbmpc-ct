#include <ct/optcon/optcon.h>
#include "infoDir.h"
#include "models/MooreGreitzerCompressor.h"
#include "plot/plotResultsMGCM.h"
#include "plot/linspace-impl.h"
/*!
 * Example ofl Direct Multiple Shooting with a Moore Greizer Compressor model,
 * using IPOPT as NLP solver.
 */
int main(int argc, char** argv)
{
    using namespace ct::optcon;
    using namespace ct::core;

    const size_t state_dim = MooreGreitzerCompressor::STATE_DIM;
    const size_t control_dim = MooreGreitzerCompressor::CONTROL_DIM;

    /**
	 * STEP 1 : set up the optimal control problem
	 * */

    StateVector<state_dim> x_0;
    StateVector<state_dim> x_final;

    x_0 << 0.15, 
        1.2875, 
        1.1547, 
        0.0;
    x_final << 0.5, 
            1.6875, 
            1.1547, 
            0.0;;

    double x2_c=(0.0);
    double beta=(1.0);
    double zeta=(1.0/sqrt(2.0));
    double wn=(sqrt(1000.0));

    // create compressor system
    std::shared_ptr<MooreGreitzerCompressor> compressor(new MooreGreitzerCompressor(x2_c, beta, zeta, wn));

    // load the cost weighting matrices from file and store them in terms. Note that we only use intermediate cost
    std::shared_ptr<ct::optcon::TermQuadratic<state_dim, control_dim>> intermediateCost(
        new ct::optcon::TermQuadratic<state_dim, control_dim>());
    intermediateCost->loadConfigFile(infoDir + "/mpcCost.info", "intermediateCost", true);

    // create a cost function and add the terms to it.
    std::shared_ptr<CostFunctionQuadratic<state_dim, control_dim>> costFunction(
        new CostFunctionAnalytical<state_dim, control_dim>());
    costFunction->addIntermediateTerm(intermediateCost);
    
    // intermedate constraints 
    // ControlVector<control_dim> u_lb = 0.1547 * ControlVector<control_dim>::Ones();
    // ControlVector<control_dim> u_ub = 2.1547 * ControlVector<control_dim>::Ones();
    // ControlVector<control_dim> u_lb = -2.0 * ControlVector<control_dim>::Ones();
    // ControlVector<control_dim> u_ub = - u_lb;
    // StateVector<state_dim> x_lb = -2.0 * ControlVector<control_dim>::Ones();
    StateVector<state_dim> x_lb;
    x_lb << 0.0, 
        1.1875, 
        0.1547, 
        -20;
    StateVector<state_dim> x_ub;
    x_ub << 1, 
        2.1875, 
        2.1547, 
        20;
    // include hard constraints
    std::shared_ptr<ct::optcon::ConstraintContainerAnalytical<state_dim, control_dim>> constraints(
        new ct::optcon::ConstraintContainerAnalytical<4, 1>());

    std::shared_ptr<ct::optcon::StateConstraint<state_dim, control_dim>> stateConstraint(
        new ct::optcon::StateConstraint<state_dim, control_dim>(x_lb, x_ub));
    stateConstraint->setName("StateConstraint");
    std::shared_ptr<TerminalConstraint<state_dim, control_dim>> terminalConstraint(
        new TerminalConstraint<state_dim, control_dim>(x_final));
    terminalConstraint->setName("TerminalConstraint");

    constraints->addIntermediateConstraint(stateConstraint, true);
    constraints->addTerminalConstraint(terminalConstraint, false);
    constraints->initialize();

    // define optcon problem and add constraint
    ContinuousOptConProblem<state_dim, control_dim> optConProblem(compressor, costFunction);
    optConProblem.setInitialState(x_0);
    optConProblem.setGeneralConstraints(constraints);

    std::cout << "=============================================" << std::endl;
    std::cout << "   Printing   out    constraints            :" << std::endl;
    std::cout << "=============================================" << std::endl;
    constraints->printout();

    /**
	 * STEP 2 : determine solver settings
	 */
    DmsSettings settings;
    settings.N_ = 25;        // number of nodes
    settings.T_ = 5.0;       // final time horizon
    settings.nThreads_ = 4;  // number of threads for multi-threading
    settings.splineType_ = DmsSettings::PIECEWISE_LINEAR;
    settings.costEvaluationType_ = DmsSettings::FULL;  // we evaluate the full cost and use no trapezoidal approximation
    settings.objectiveType_ = DmsSettings::KEEP_TIME_AND_GRID;  // don't optimize the time spacing between the nodes
    settings.h_min_ = 0.1;                         // minimum admissible distance between two nodes in [sec]
    settings.integrationType_ = DmsSettings::RK4;  // type of the shot integrator
    settings.dt_sim_ = 0.01;                       // forward simulation dt
    settings.solverSettings_.solverType_ = NlpSolverType::IPOPT;  // use IPOPT
    settings.absErrTol_ = 1e-8;
    settings.relErrTol_ = 1e-8;

    settings.print();

    /**
	 * STEP 3 : Calculate an appropriate initial guess
	 */
    StateVectorArray<state_dim> x_initguess;
    ControlVectorArray<control_dim> u_initguess;
    DmsPolicy<state_dim, control_dim> initialPolicy;

    x_initguess.resize(settings.N_ + 1, StateVector<state_dim>::Zero());
    u_initguess.resize(settings.N_ + 1, ControlVector<control_dim>::Zero());
    for (size_t i = 0; i < settings.N_ + 1; ++i)
    {
        x_initguess[i] = x_0 + (x_final - x_0) * (i / settings.N_);
    }

    /**
     * STEP 4: solve DMS via IPOPT
        */
    optConProblem.setTimeHorizon(settings.T_);
    std::shared_ptr<DmsSolver<state_dim, control_dim>> dmsSolver(
    new DmsSolver<state_dim, control_dim>(optConProblem, settings));

    initialPolicy.xSolution_ = x_initguess;
    initialPolicy.uSolution_ = u_initguess;
    dmsSolver->setInitialGuess(initialPolicy);

    dmsSolver->solve();
        
    // retrieve the solution
    DmsPolicy<state_dim, control_dim> solution = dmsSolver->getSolution();
    // plot
    plotResultsMGCM<state_dim, control_dim>(solution.xSolution_, solution.uSolution_, solution.tSolution_);
    return 1;
}
