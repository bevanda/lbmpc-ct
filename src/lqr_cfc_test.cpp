#include <ct/optcon/optcon.h>  // also includes ct_core
#include "infoDir.h"
#include "models/MooreGreitzerCompressor.h"
#include "plot/linspace-impl.h"
/////////////////////////////////////
/* CONSTANT STATE FEEDBACK CONTROL */
/////////////////////////////////////

// Parameter values for calculating  feedback gain K for the linearized CLTI system
namespace calc { 
    ct::core::ADCGScalar x2_c(0.0);
    ct::core::ADCGScalar beta(1.0);
    ct::core::ADCGScalar zeta(1.0/sqrt(2.0));
    ct::core::ADCGScalar wn(sqrt(1000.0));
}
// Parameter values for simulating the controlled MGCM plant
namespace sim {
    double x2_c=(0.0);
    double beta=(1.0);
    double zeta=(1.0/sqrt(2.0));
    double wn=(sqrt(1000.0));
}

int main(int argc, char** argv)
{
    /*** Feedback gain calculation via Auto-Diff ***/
    // get the state and control input dimension of the mgcm
    const size_t state_dim = ct::core::MooreGreitzerCompressor::STATE_DIM;
    const size_t control_dim = ct::core::MooreGreitzerCompressor::CONTROL_DIM;
    // create an auto-differentiable instance of the mgcm dynamics
    // create a MGCM instance
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim, ct::core::ADCGScalar>> mgcmDynamicsCALC(
        new ct::core::tpl::MooreGreitzerCompressor<ct::core::ADCGScalar>(calc::x2_c, calc::beta, calc::zeta, calc::wn));
    // create an Auto-Differentiation Linearizer with code generation on the quadrotor model
    ct::core::ADCodegenLinearizer<state_dim, control_dim> adLinearizer(mgcmDynamicsCALC);
    // compile the linearized model just-in-time
    adLinearizer.compileJIT();
    // define the linearization point around steady state
    ct::core::StateVector<state_dim> x_ref;
    x_ref << 0.5, 1.6875, 1.1547, 0.0;
    ct::core::ControlVector<control_dim> u;
    u(0) = 1.1547;
    double t = 0.0;
    // compute the linearization around the nominal state using the Auto-Diff Linearizer
    auto A = adLinearizer.getDerivativeState(x_ref, u, t);
    auto B = adLinearizer.getDerivativeControl(x_ref, u, t);
    // B << 0,0,0,1000; // manually changed, bc AD wrong calculation
    // load the weighting matrices
    ct::optcon::TermQuadratic<state_dim, control_dim> quadraticCost;
    quadraticCost.loadConfigFile(infoDir+"/LQcost.info", "termLQR");
    auto Q = quadraticCost.stateSecondDerivative(x_ref, u, t);    // x_ref, u and t can be arbitrary here
    auto R = quadraticCost.controlSecondDerivative(x_ref, u, t);  // x_ref, u and t can be arbitrary here
    // design the LQR controller
    ct::optcon::LQR<state_dim, control_dim> lqrSolver;
    ct::core::FeedbackMatrix<state_dim, control_dim> K;
    std::cout << "deltaA: " << std::endl << A << std::endl << std::endl;
    std::cout << "deltaB: " << std::endl << B << std::endl << std::endl;
    std::cout << "deltaQ: " << std::endl << Q << std::endl << std::endl;
    std::cout << "deltaR: " << std::endl << R << std::endl << std::endl;
    lqrSolver.compute(Q, R, A, B, K);
    std::cout << "LQR gain matrix:" << std::endl << K << std::endl;
    std::cout << "\n" << std::endl;

    /*** SIMULATION ***/
    ct::core::ControlVector<control_dim> uff;
    uff << 1.1547;
    // Set up simulaion of the system
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim>> mgcmDynamicsSIM(
    new ct::core::MooreGreitzerCompressor(sim::x2_c, sim::beta, sim::zeta, sim::wn));
    // create a controller
    // *caution!*: since u = u_ff + K*x; the K calculated via lqrSolver.compute(Q, R, A, B, K) must have a negative sign
    std::shared_ptr<ct::core::ConstantStateFeedbackController<state_dim, control_dim>> controller(
        new ct::core::ConstantStateFeedbackController<state_dim, control_dim>(uff, x_ref, -K));
    // assign our controller
    mgcmDynamicsSIM->setController(controller);

    // create an integrator
    ct::core::Integrator<state_dim> integrator(mgcmDynamicsSIM);
    // inital state
    ct::core::StateVector<state_dim> x;
    x << (0.15), 
        (1.2875), 
        (1.1547), 
        (0.0);
    // Simulation setup
    std::size_t length = 1000; 
    // vectors for relevant states
    std::vector<double> state1(length, 0.0);
    std::vector<double> state2(length, 0.0);
    std::vector<double> state3(length, 0.0);
    std::vector<double> state4(length, 0.0);
    std::vector<double> input(length, 0.0);
    ct::core::ControlVector<control_dim> inVec;
    ct::core::Time t0 = 0.0;
    double dt = 0.001;
    size_t nSteps = 10;
    for (size_t i = 0; i < length; i++)
    {
        inVec=mgcmDynamicsSIM->getLastControlAction();
        integrator.integrate_n_steps(x, t0, nSteps, dt);
        // add the state tos tates
        state1[i]=x(0);
        state2[i]=x(1);
        state3[i]=x(2);
        state4[i]=x(3);
        if (i==0)
        {
             input[i]=1.1547;
        }
        else
        {
            input[i]=inVec(0);
        }
        
        // print the new state
        std::cout << "state after integration: " << x.transpose() << std::endl;
    }

    // plotting
    double tend=dt*length;
    std::vector<double> times = LinearSpacedArray(t0, tend, length);

    ct::core::plot::subplot(5, 1, 1);
    ct::core::plot::plot(times, state1);
    ct::core::plot::subplot(5, 1, 2);
    ct::core::plot::plot(times, state2);
    ct::core::plot::subplot(5, 1, 3);
    ct::core::plot::plot(times, state3);;
    ct::core::plot::subplot(5, 1, 4);
    ct::core::plot::plot(times, state4);
    ct::core::plot::subplot(5, 1, 5);
    ct::core::plot::plot(times, input);
    ct::core::plot::show();

    return 0;
}