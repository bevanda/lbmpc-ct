#include <ct/optcon/optcon.h>  // also includes ct_core
#include "infoDir.h"
#include "models/MooreGreitzerCompressor.h"

/* VARIABLE STATE FEEDBACK CONTROL */

// Parameter values for calculating  feedback gain K for the linearized CLTI system
namespace calc { 
    ct::core::ADCGScalar x2_c(0.0);
    ct::core::ADCGScalar beta(1.0);
    ct::core::ADCGScalar zeta(1.0/sqrt(2.0));
    ct::core::ADCGScalar wn(sqrt(1000.0));
}
// Parameter values for simulating the controlled MGCM plant
namespace sim {
    double x2_c=(0.0);
    double beta=(1.0);
    double zeta=(1.0/sqrt(2.0));
    double wn=(sqrt(1000.0));
}

int main(int argc, char** argv)
{
    
    // get the state and control input dimension of the mgcm
    const size_t state_dim = ct::core::MooreGreitzerCompressor::STATE_DIM;
    const size_t control_dim = ct::core::MooreGreitzerCompressor::CONTROL_DIM;

    // create an auto-differentiable instance of the mgcm dynamics
    // create a MGCM instance

    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim, ct::core::ADCGScalar>> mgcmDynamicsCALC(
        new ct::core::tpl::MooreGreitzerCompressor<ct::core::ADCGScalar>(calc::x2_c, calc::beta, calc::zeta, calc::wn));
    // create an Auto-Differentiation Linearizer with code generation on the quadrotor model
    ct::core::ADCodegenLinearizer<state_dim, control_dim> adLinearizer(mgcmDynamicsCALC);
    // compile the linearized model just-in-time
    adLinearizer.compileJIT();
    // define the linearization point around steady state
    ct::core::StateVector<state_dim> x_ref;
    x_ref << 0.5, 1.6875, 1.1547, 0.0;
    ct::core::ControlVector<control_dim> u;
    u(0) = 1.1547;
    double t = 0.0;
    // compute the linearization around the nominal state using the Auto-Diff Linearizer
    auto A = adLinearizer.getDerivativeState(x_ref, u, t);
    auto B = adLinearizer.getDerivativeControl(x_ref, u, t);
    // B << 0,0,0,1000; // manually changed, bc AD wrong calculation
    // load the weighting matrices
    ct::optcon::TermQuadratic<state_dim, control_dim> quadraticCost;
    quadraticCost.loadConfigFile(infoDir+"/LQcost.info", "termLQR");
    auto Q = quadraticCost.stateSecondDerivative(x_ref, u, t);    // x_ref, u and t can be arbitrary here
    auto R = quadraticCost.controlSecondDerivative(x_ref, u, t);  // x_ref, u and t can be arbitrary here
    // design the LQR controller
    ct::optcon::LQR<state_dim, control_dim> lqrSolver;
    ct::core::FeedbackMatrix<state_dim, control_dim> K;
    std::cout << "deltaA: " << std::endl << A << std::endl << std::endl;
    std::cout << "deltaB: " << std::endl << B << std::endl << std::endl;
    std::cout << "deltaQ: " << std::endl << Q << std::endl << std::endl;
    std::cout << "deltaR: " << std::endl << R << std::endl << std::endl;
    lqrSolver.compute(Q, R, A, B, K);
    std::cout << "LQR gain matrix:" << std::endl << K << std::endl;
    std::cout << "\n" << std::endl;
    // Set up simulation
 
    double dt = 0.01; // discretiztion time
    size_t N = 50; // time horizon
    ct::core::ControlVector<control_dim> uff;
    uff << 1.1547;
    // Set up simulaion of the system
    std::shared_ptr<ct::core::ControlledSystem<state_dim, control_dim>> mgcmDynamicsSIM(
    new ct::core::MooreGreitzerCompressor(sim::x2_c, sim::beta, sim::zeta, sim::wn));
   
    ct::core::StateVector<state_dim> x;
    x << (0.15), 
        (1.2875), 
        (1.1547), 
        (0.0);
    ct::core::StateVectorArray<state_dim> x_ref_init(N + 1, x); 
    ct::core::ControlVectorArray<control_dim> u0_ff(N, uff);
    ct::core::FeedbackArray<state_dim, control_dim> u0_fb(N, -K);
    // create a controller
    // *caution!*: since u = u_ff + K*x; the K calculated via lqrSolver.compute(Q, R, A, B, K) must have a negative sign
    std::shared_ptr<ct::core::StateFeedbackController<state_dim, control_dim>> controller(
        // new ct::core::StateFeedbackController<state_dim, control_dim>(uff, x_ref, -K));
        new ct::core::StateFeedbackController<state_dim, control_dim>(x_ref_init, u0_ff, u0_fb, dt));
    // assign our controller
    mgcmDynamicsSIM->setController(controller);

    // create an integrator
    ct::core::Integrator<state_dim> integrator(mgcmDynamicsSIM);
    // inital state
    // for (size_t i = 0; i < 500; i++)
    // {
    //     ct::core::Time t0 = 0.0;
    //     // double dt = 0.001;
    //     size_t nSteps = 10;
    //     integrator.integrate_n_steps(x, t0, nSteps, dt);
    //     // print the new state
    //     std::cout << "state after integration: " << x.transpose() << std::endl;
    // }
    

    return 0;
}