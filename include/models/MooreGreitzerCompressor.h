/**********************************************************************************************************************
This file is part of the Control Toolbox (https://github.com/ethz-adrl/control-toolbox), copyright by ETH Zurich.
Licensed under the BSD-2 license (see LICENSE file in main directory)
**********************************************************************************************************************/

#pragma once
#include <ct/core/core.h>

namespace ct {
namespace core {
namespace tpl {
    
template <typename SCALAR>
class MooreGreitzerCompressor : public ControlledSystem<4, 1, SCALAR>
{
public:
    static const size_t STATE_DIM = 4;    //!< state dimension (position, velocity)
    static const size_t CONTROL_DIM = 1;  //!< control dimension (force)

    typedef ControlledSystem<STATE_DIM, CONTROL_DIM, SCALAR> Base;
    typedef typename Base::time_t time_t;

    //! default constructor
    MooreGreitzerCompressor() = delete;

    //! constructor directly using frequency and damping coefficients
    /*!
	 *
	 * @param w_n eigenfrequency
	 * @param zeta damping ratio
	 * @param g_dc DC gain on input, set to \f$ w_n^2 \f$ to achieve amplification 1
	 * @param controller controller (optional)
	 */
    MooreGreitzerCompressor(SCALAR x2_c = SCALAR(0.0),
        SCALAR beta = SCALAR(1.0), 
        SCALAR zeta = SCALAR(1.0/sqrt(2.0)), 
        SCALAR wn = SCALAR(sqrt(1000.0)),
        SCALAR wn_squared = SCALAR(1000.0),
        std::shared_ptr<Controller<STATE_DIM, CONTROL_DIM, SCALAR>> controller = nullptr)
        : ControlledSystem<STATE_DIM, CONTROL_DIM, SCALAR>(controller),
        x2_c_(x2_c), 
        beta_(beta),
        zeta_(zeta),
        wn_(wn),
        wn_squared_(wn_squared)
    {
    }

    //! copy constructor
    MooreGreitzerCompressor(const MooreGreitzerCompressor& arg)
        : ControlledSystem<STATE_DIM, CONTROL_DIM, SCALAR>(arg),
        x2_c_(arg.x2_c_),
        beta_(arg.beta_),
        wn_(arg.wn_),
        zeta_(arg.zeta_),
        wn_squared_(arg.wn_squared_)
    {
    }

    //! deep copy
    MooreGreitzerCompressor* clone() const override { return new MooreGreitzerCompressor(*this); }
    //! destructor
    virtual ~MooreGreitzerCompressor() {}
    //! set the dynamics
    /*!
	 * @param w_n eigenfrequency
	 * @param zeta damping ratio
	 * @param g_dc DC gain
	 */
    void setDynamics(SCALAR x2_c = SCALAR(0.0),
        SCALAR beta = SCALAR(1.0), 
        SCALAR zeta = SCALAR(1.0/sqrt(2.0)), 
        SCALAR wn =SCALAR(sqrt(1000.0)))
    {
        x2_c = x2_c;
        beta_= beta;
        zeta_ = zeta;
        wn_ = wn;
        wn_squared_ = wn_ * wn_;
    }

    //! evaluate the system dynamics
    /*!
	 * @param state current state (position, velocity)
	 * @param t current time (gets ignored)
	 * @param control control action
	 * @param derivative derivative (velocity, acceleration)
	 */
    void computeControlledDynamics(const StateVector<STATE_DIM, SCALAR>& state,
        const time_t& t,
        const ControlVector<CONTROL_DIM, SCALAR>& control,
        StateVector<STATE_DIM, SCALAR>& derivative) override
    {
        // a a Moore-Greitzer compressor model has 4 states:
        //  mass flow, pressure rise, throttle input, throttle rate
        /*         
        f(1) = -state(2)+x2_c+1+3*(state(1)/2)-(state(1)^3/2);  % mass flow rate
        f(2) = (state(1)+1-state(3)*sqrt(state(2)))/(beta^2);   % pressure rise rate
        f(3) = state(4);                                % throttle opening rate
        f(4) = -wn^2*state(3)-2*zeta*wn*state(4)+wn^2*u;    % throttle opening acceleration 
        */
       
        // first part of state derivative is mass flow
        derivative(0) = -state(1) + x2_c_ + 1.0 + 3.0*(state(0)/2.0) - ((state(0)*state(0)*state(0))/2.0);
        // second part is pressure rise
        derivative(1) = (state(0) + 1.0 - state(2)*sqrt(state(1.0))) / (beta_*beta_);
        // third part is throttle
        derivative(2) = state(3);
        // fourth part is throttle rate
        derivative(3) = -wn_squared_*state(2) - 2.0*zeta_*wn_*state(3) + wn_squared_*control(0);
    }


    //! print out infos about the system
    void printSystemInfo()
    {
        std::cout << "Mass flow constant: " << x2_c_ << std::endl;
        std::cout << "Beta: " << beta_ << std::endl;
        std::cout << "Zeta: " << zeta_ << std::endl;
        std::cout << "Natural frequency: " << wn_ << std::endl;
    }

private:
    SCALAR x2_c_;
    SCALAR beta_;
    SCALAR zeta_;
    SCALAR wn_;
    SCALAR wn_squared_;  //!< eigenfrequency squared
};

}  // namespace tlp

typedef tpl::MooreGreitzerCompressor<double> MooreGreitzerCompressor;  //!< for the template backward compaibilty

}  // namespace core
}  // namespace ct