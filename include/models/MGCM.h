#pragma once
#include <ct/core/core.h>  // as usual, include CT
// #include <math.h>
// A Moore-Greitzer Compressor model class that derives from ct::core::System
class MGCM : public ct::core::ControlledSystem<4,1>
{
public:
    static const size_t STATE_DIM = 4;
    static const size_t CTRL_DIM = 1;

    // constructor
    MGCM(double x2_c, double beta, double zeta, double wn, double u_init) : 
        x2_c_(x2_c), 
        beta_(beta),
        zeta_(zeta),
        wn_(wn) {controlAction_ << u_init;}
    // copy constructor
    MGCM(const MGCM& other) :         
        x2_c_(other.x2_c_), 
        beta_(other.beta_),
        zeta_(other.zeta_),
        wn_(other.wn_),
        controlAction_(other.controlAction_) {/*empty*/}
    // destructor
    ~MGCM() {/*empty*/}
    // clone method (must have same declaration as the pure virtual deep copy from the base class)
    MGCM* clone() const override
    {
        return new MGCM(*this);  // calls copy constructor
    }
    // Controlled system dynamics. Implemented here since it is pure virtual in the base function.
    void computeControlledDynamics(const ct::core::StateVector<STATE_DIM>& x,
        const time_t& t,
        const ct::core::ControlVector<CTRL_DIM>& u,
        ct::core::StateVector<STATE_DIM>& derivative) override
    {
        /*         
        f(1) = -x(2)+x2_c+1+3*(x(1)/2)-(x(1)^3/2);  % mass flow rate
        f(2) = (x(1)+1-x(3)*sqrt(x(2)))/(beta^2);   % pressure rise rate
        f(3) = x(4);                                % throttle opening rate
        f(4) = -wn^2*x(3)-2*zeta*wn*x(4)+wn^2*u;    % throttle opening acceleration 
        */
        // first part of state derivative is mass flow
        derivative(0) = -x(1) + x2_c_ + 1.0 + 3.0*(x(0)/2.0) - (pow(x(0),3.0)/2.0);
        // second part is pressure rise
        derivative(1) = (x(0) + 1.0 - x(2)*sqrt(x(1.0))) / (pow(beta_,2.0));
        // third part is throttle
        derivative(2) = x(3);
        // fourth part is throttle rate
        derivative(3) = -pow(wn_,2.0)*x(2) - 2.0*zeta_*wn_*x(3) + pow(wn_,2.0)*u(0);
    }
private:
    double x2_c_;
    double beta_;
    double zeta_;
    double wn_;
    ct::core::ControlVector<CTRL_DIM> controlAction_;
};

