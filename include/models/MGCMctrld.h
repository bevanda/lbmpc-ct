#pragma once
#include "ct/optcon/optcon.h"

// namespace sys{
namespace mdl {

template <typename SCALAR>
class MGCM : public  ct::core::ControlledSystem<4, 1, SCALAR>
{
public:
    static const size_t STATE_DIM = 4;    //!< state dimension (position, velocity)
    static const size_t CONTROL_DIM = 1;  //!< control dimension (force)

    //! constructor directly using frequency and damping coefficients
    /*!
	 *
	 * @param wneigenfrequency
	 * @param zeta damping ratio
	 * @param g_dc DC gain on input, set to \f$ wn^2 \f$ to achieve amplification 1
	 * @param controller controller (optional)
	 */
    MGCM(SCALAR x2_c = SCALAR(0.0),
     SCALAR beta = SCALAR(1.0), 
     SCALAR zeta = SCALAR(1.0/sqrt(2.0)), 
     SCALAR wn =SCALAR(sqrt(1000.0)), 
        std::shared_ptr<ct::core::Controller<4, 1, SCALAR>> controller = nullptr)
        : ct::core::ControlledSystem<4, 1, SCALAR>(controller),
        x2_c_(x2_c), 
        beta_(beta),
        zeta_(zeta),
        wn_(wn)
    {
    }

    //! copy constructor
    MGCM(const MGCM& arg)
        : ct::core::ControlledSystem<4, 1, SCALAR>(arg),
          wn_(arg.wn_),
          wn_square_(arg.wn_square_),
          zeta_(arg.zeta_),
          beta_(arg.beta_),
          x2_c_(arg.x2_c_)
    {
    }

    //! deep copy
    virtual MGCM* clone() const override { return new MGCM(*this); }
    //! destructor
    virtual ~MGCM() {}
    //! set the dynamics
    /*!
	 * @param wn eigenfrequency
	 * @param zeta damping ratio
	 * @param g_dc DC gain
	 */
    void setDynamics(SCALAR x2_c = SCALAR(0.0),
     SCALAR beta = SCALAR(1.0), 
     SCALAR zeta = SCALAR(1.0/sqrt(2.0)), 
     SCALAR wn =SCALAR(sqrt(1000.0)))
    {
        x2_c_ = x2_c;
        wn_ = wn;
        wn_square_ = wn_ * wn_;
        zeta_ = zeta;
        beta_ = beta;
    }

    //! evaluate the system dynamics
    /*!
	 * @param state current state (position, velocity)
	 * @param t current time (gets ignored)
	 * @param control control action
	 * @param derivative derivative (velocity, acceleration)
	 */
    void computeControlledDynamics(const ct::core::StateVector<STATE_DIM, SCALAR>& state,
        const time_t& t,
        const ct::core::ControlVector<CONTROL_DIM, SCALAR>& control,
        ct::core::StateVector<STATE_DIM, SCALAR>& derivative) override
    {
        // a a Moore-Greitzer compressor model has 4 states:
        //  mass flow, pressure rise, throttle input, throttle rate
        /*         
        f(1) = -state(2)+x2_c+1+3*(state(1)/2)-(state(1)^3/2);  % mass flow rate
        f(2) = (state(1)+1-state(3)*sqrt(state(2)))/(beta^2);   % pressure rise rate
        f(3) = state(4);                                % throttle opening rate
        f(4) = -wn^2*state(3)-2*zeta*wn*state(4)+wn^2*u;    % throttle opening acceleration 
        */
       
        // first part of state derivative is mass flow
        derivative(0) = -state(1) + x2_c_ + 1.0 + 3.0*(state(0)/2.0) - (pow(state(0),3.0)/2.0);
        // second part is pressure rise
        derivative(1) = (state(0) + 1.0 - state(2)*sqrt(state(1.0))) / (pow(beta_,2.0));
        // third part is throttle
        derivative(2) = state(3);
        // fourth part is throttle rate
        derivative(3) = -wn_square_*state(2) - 2.0*zeta_*wn_*state(3) + wn_square_*control(0);
    }

    //! check the parameters
    /*!
	 * @return true if parameters are physical
	 */
    
    //! print out infos about the system
    void printSystemInfo()
    {
        std::cout << "Mass flow constant: " << x2_c_ << std::endl;
        std::cout << "Beta: " << beta_ << std::endl;
        std::cout << "Zeta: " << zeta_ << std::endl;
        std::cout << "Natural frequency: " << wn_ << std::endl;
    }

private:
    SCALAR wn_square_;  //!< eigenfrequency squared
    SCALAR x2_c_;
    SCALAR beta_;
    SCALAR zeta_;
    SCALAR wn_;
};

}  // namespace mdl
using MGCM = mdl::MGCM<double>; // this line ensures backwards compatibility of dependent code
// }  // namespace sys