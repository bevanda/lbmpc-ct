#pragma once
#include "models/TPL_test.h"

namespace my_sys {
namespace tpl {

template <typename SCALAR>
MySystem::MooreGreitzerCompressor(SCALAR w_n,
        SCALAR zeta = SCALAR(1.0),
        SCALAR g_dc = SCALAR(1.0),
        std::shared_ptr<Controller<2, 1, SCALAR>> controller = nullptr)
        : ct::core::ControlledSystem<2, 1, SCALAR>(controller, SYSTEM_TYPE::SECOND_ORDER),
          w_n_(w_n),
          w_n_square_(w_n_ * w_n_),
          zeta_(zeta),
          g_dc_(g_dc)
    {
    }
//! copy constructor
template <typename SCALAR>
MySystem::MooreGreitzerCompressor(const MooreGreitzerCompressor& arg)
        : ct::core::ControlledSystem<2, 1, SCALAR>(arg),
          w_n_(arg.w_n_),
          w_n_square_(arg.w_n_square_),
          zeta_(arg.zeta_),
          g_dc_(arg.g_dc_)
    {
    }
//! deep copy
template <typename SCALAR>
MySystem::MooreGreitzerCompressor* clone() const override { return new MooreGreitzerCompressor(*this); }
//! destructor
template <typename SCALAR>
virtual MySystem::~MooreGreitzerCompressor() {}
template <typename SCALAR>
void MySystem::setDynamics(SCALAR w_n, SCALAR zeta = SCALAR(1.0), SCALAR g_dc = SCALAR(1.0))
{
    w_n_ = w_n;
    w_n_square_ = w_n_ * w_n_;
    zeta_ = zeta;
    g_dc_ = g_dc;
}
template <typename SCALAR>
void MySystem::computeControlledDynamics(const ct::core::StateVector<STATE_DIM, SCALAR>& state,
        const SCALAR& t,
        const ct::core::ControlVector<CONTROL_DIM, SCALAR>& control,
        ct::core::StateVector<STATE_DIM, SCALAR>& derivative) override 
        {
            derivative(0) = state(1);
            derivative(1) = g_dc_ * control(0) - 2.0 * zeta_ * w_n_ * state(1) - w_n_square_ * state(0);
        }

} // namespace tpl
} // namespace my_sys