#pragma once
#include <ct/core/core.h>

namespace my_sys {
namespace tpl {
template <typename SCALAR>
class MySystem : ct::core::ControlledSystem<2, 1, SCALAR> {
public:
    size_t STATE_DIM = 2;
    size_t INPUT_DIM = 1;
    //! default constructor
    MySystem(); 
    //! constructor with params
    MySystem(SCALAR w_n,
        SCALAR zeta = SCALAR(1.0),
        SCALAR g_dc = SCALAR(1.0),
        std::shared_ptr<ct::core::Controller<2, 1, SCALAR>> controller = nullptr);
    //! copy constructor
    MooreGreitzerCompressor(const MooreGreitzerCompressor& arg);
    //! deep copy
    MooreGreitzerCompressor* clone() const override;
    //! destructor
    virtual ~MooreGreitzerCompressor();
    void setDynamics(SCALAR w_n, SCALAR zeta = SCALAR(1.0), SCALAR g_dc = SCALAR(1.0));
    void computeControlledDynamics(const ct::core::StateVector<STATE_DIM, SCALAR>& state,
        const SCALAR& t,
        const ct::core::ControlVector<CONTROL_DIM, SCALAR>& control,
        ct::core::StateVector<STATE_DIM, SCALAR>& derivative) override;
private:
    SCALAR w_n_;         //!< eigenfrequency
    SCALAR w_n_square_;  //!< eigenfrequency squared
    SCALAR zeta_;        //!< damping ratio
    SCALAR g_dc_;        //!< input DC gain
};
} // namespace tpl
using MySystem = tpl::MySystem<double>; // this line ensures backwards compatibility of dependent code
} // namespace my_sys