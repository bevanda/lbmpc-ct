#pragma once
#include<ct/core/core.h>

template <size_t STATE_DIM, size_t CONTROL_DIM>
void plotResultsMGCM(const ct::core::StateVectorArray<STATE_DIM>& stateArray,
    const ct::core::ControlVectorArray<CONTROL_DIM>& controlArray,
    const ct::core::TimeArray& timeArray)
{
#ifdef PLOTTING_ENABLED

    using namespace ct::core;

    try
    {
        plot::ion();
        plot::figure();

        if (timeArray.size() != stateArray.size())
        {
            std::cout << timeArray.size() << std::endl;
            std::cout << stateArray.size() << std::endl;
            throw std::runtime_error("Cannot plot data, x and t not equal length");
        }

        std::vector<double> x1;
        std::vector<double> x2;
        std::vector<double> x3;
        std::vector<double> x4;
        std::vector<double> time_state;
        for (size_t j = 0; j < stateArray.size(); j++)
        {
            x1.push_back(stateArray[j](0));
            x2.push_back(stateArray[j](1));
            x3.push_back(stateArray[j](2));
            x4.push_back(stateArray[j](3));
            time_state.push_back(timeArray[j]);
        }

        std::vector<double> control;
        std::vector<double> time_control;
        for (size_t j = 0; j < controlArray.size(); j++)
        {
            control.push_back(controlArray[j](0));
            time_control.push_back(timeArray[j]);
        }

        plot::subplot(5, 1, 1);
        plot::plot(time_state, x1);
        plot::title("mass flow");

        plot::subplot(5, 1, 2);
        plot::plot(time_state, x2);
        plot::title("pressure rise");

        plot::subplot(5, 1, 3);
        plot::plot(time_state, x3);
        plot::title("throttle");

        plot::subplot(5, 1, 4);
        plot::plot(time_state, x4);
        plot::title("throttle rate");

        plot::subplot(5, 1, 5);
        plot::plot(time_control, control);
        plot::title("control");

        plot::show();
    } catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }
#else
    std::cout << "Plotting is disabled." << std::endl;
#endif
}
